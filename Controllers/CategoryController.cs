﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoppingWebsite.Controllers
{
    public class CategoryController : Controller
    {
        public ActionResult Computer()
        {
            ViewBag.Title = "電腦相關";
            GetProductByCategory(1);
            return View();
        }

        public void GetProductByCategory(int n)
        {
            var url = "http://10.11.24.95/Intern/Product/SelectByCategory/"+n;
            var response = new getApi().PostApi("", url);
            var r = response.Content.ReadAsStringAsync();
            JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);
            var count = jObject["Result"].Count();
            ViewBag.Count = count;
            for (int i = 1; i <= count; i++)
            {
                ViewData["ProductName" + i] = jObject["Result"][i - 1]["Name"];
                ViewData["ProductPrice" + i] = jObject["Result"][i - 1]["Price"];
                ViewData["ProductUri" + i] = jObject["Result"][i - 1]["DefaultImageId"];
                ViewData["ProductId"+i] = jObject["Result"][i - 1]["Id"];
            }
        }

        [HttpPost]
        public ActionResult AddToCart(string ProductId)
        {
            if (Session["Id"] != null)
            {
                string url = "http://10.11.24.95/Intern/Cart/Add";
                var response = new getApi().PostApiWithHeader("", url, Session["Id"].ToString(), ProductId);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);
                    System.Windows.Forms.MessageBox.Show("已加入購物車", "成功");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult ComsumerElec()
        {
            ViewBag.Title = "3C";
            GetProductByCategory(2);
            return View();
        }

        public ActionResult HomeAppliance()
        {
            ViewBag.Title = "家電";
            GetProductByCategory(3);
            return View();
        }

        public ActionResult Daily()
        {
            ViewBag.Title = "日常用品";
            GetProductByCategory(4);
            return View();
        }

        public ActionResult Lifes()
        {
            ViewBag.Title = "生活";
            GetProductByCategory(5);
            return View();
        }
        public ActionResult Beauty()
        {
            ViewBag.Title = "美妝";
            GetProductByCategory(6);
            return View();
        }

        public ActionResult Food()
        {
            ViewBag.Title = "食品";
            GetProductByCategory(7);
            return View();
        }
    }
}