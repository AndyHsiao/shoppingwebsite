﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace ShoppingWebsite.Controllers
{
    public class getApi
    {
        public HttpResponseMessage PostApi(string data, string url)
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            return response;
        }

        public HttpResponseMessage PostApiWithHeader(string data, string url, string UserId, string ProductId)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("UserId", UserId);
            client.DefaultRequestHeaders.Add("ProductId", ProductId);
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            return response;
        }

        public HttpResponseMessage PostApiFormDataAsync(string data, string url)
        {
            HttpClient client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent(data, Encoding.UTF8, "multipart/form-data");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            return response;
        }

        public HttpResponseMessage GetMemberInfo(int id)
        {
            string url = "http://10.11.24.95/Intern/User/Select/" + id;
            HttpClient client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            return response;
        }

        public void CountClick(string id)
        {
            string url = "http://10.11.24.95/Intern/SalesRank/Click/" + id;
            
            HttpClient client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent("", Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            if (response.StatusCode.ToString() == "OK")
            {
                var r = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                //ViewBag.Name = jObject["Result"]["Name"];

            }
            else
            {
                Debug.WriteLine(response.StatusCode);
            }
        }

        public string SalesRank(int i)
        {
            string url = "http://10.11.24.95/Intern/SalesRank/Rank";

            string StartDate = "2020-05-08";
            string EndDate = "2020-05-27";
            List<int> CompareProductList = new List<int>() { 3, 4, 5, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044,1045, 1046, 1047, 1048, 1049, 1050, 1051, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070};

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("{");
            sb.AppendLine("\"StartDate\":\"" + StartDate + "\",");
            sb.AppendLine("\"EndDate\":\"" + EndDate + "\",");
            sb.AppendLine("\"CompareProductList\":[");
            foreach (int item in CompareProductList)
            {
                sb.Append(item + ",");
            }
            sb = new StringBuilder(sb.ToString().TrimEnd(','));//移除最後一個「,」字元
            sb.AppendLine("]");
            sb.AppendLine("}");

            HttpClient client = new HttpClient();
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent(sb.ToString(), Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            if (response.StatusCode.ToString() == "OK")
            {
                var re = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<JObject>(re.Result);
                var a = jObject["Result"][i]["Id"];
                return a.ToString();
            }
            else
            {
                Debug.WriteLine("Response : " + response.Content.ReadAsStringAsync());
                return "";
            }
        }

         
    }
}