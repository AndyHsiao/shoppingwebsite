﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Windows.Forms;

namespace ShoppingWebsite.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult ProductContent(string id)
        {
            if (id != null)
            {
                GetProductInfo(id);
                new getApi().CountClick(id);
                ViewBag.ProductId = id;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult AddToCart(string ProductId)
        {
            if (Session["Id"] != null)
            {
                string url = "http://10.11.24.95/Intern/Cart/Add";
                var response = new getApi().PostApiWithHeader("", url, Session["Id"].ToString(), ProductId);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);
                    MessageBox.Show("已加入購物車", "成功");
                    return Redirect("~/Product/ProductContent/" + ProductId);
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        public void GetProductInfo(string id)
        {
            try
            {
                var x = "";
                string JsonString = JsonConvert.SerializeObject(x);
                string url = "http://10.11.24.95/Intern/Product/Select/" + id;
                var response = new getApi().PostApi(JsonString, url);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                    ViewBag.Name = jObject["Result"]["Name"];
                    ViewBag.Description = jObject["Result"]["Description"];
                    ViewBag.Quantity = jObject["Result"]["Quantity"].ToString();
                    ViewBag.Price = jObject["Result"]["Price"];
                    ViewBag.CategoryId = jObject["Result"]["CategoryId"];
                    ViewBag.ImageId = jObject["Result"]["DefaultImageId"].ToString();
                    ViewBag.PublishDate = jObject["Result"]["PublishDate"].ToString();
                    ViewBag.Status = jObject["Result"]["Status"].ToString();
                    ViewBag.Title = jObject["Result"]["Name"];
                    ViewBag.CategoryName = GetCategoryName(jObject["Result"]["CategoryId"].ToString());
                }
                else
                {
                    Debug.WriteLine(response.StatusCode);
                }
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [HttpPost]
        public ActionResult PostForm(string ProductId)
        {
            string url = string.Format("http://10.11.24.95/Intern/Cart/Add/{0}/{1}", ProductId, Session["Id"]);
            var response = new getApi().PostApi("", url);
            if (response.StatusCode.ToString() == "OK")
            {
                var r = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);

                return RedirectToAction("Cart", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public string GetImage(string productId)
        {
            var client = new RestClient("http://10.11.24.95/Intern/Product/Select/" + productId);
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(response.Content);
            var imgName = jObject["Result"]["DefaultImageId"];
            if (imgName.ToString() == null)
            {
                return "";
            }
            else
            {
                return imgName.ToString();
            }
        }

        public ActionResult ProductEdit(string id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Id = id;
                ViewBag.Title = "商品編輯";
                GetProductInfo(id);
                return View();
            }
        }

        [HttpPost]
        public ActionResult ProductEdit(System.Web.Mvc.FormCollection form, HttpPostedFileBase file)
        {
            if (file!= null)
            {
                if (PostData(form, file))
                {
                    ViewBag.Msg = "修改成功";
                    return RedirectToAction("ProductManagement", "Member");
                }
                else
                {

                    return View(form);
                }
            }
            else
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        using (var formDataContent = new MultipartFormDataContent())
                        {
                            string getImageName = GetImage(form["Id"]);
                            string id = form["Id"];
                            string url = string.Format("http://10.11.24.95/Intern/Image/{0}", getImageName);
                            var image = DownloadImageFromUrl(url);
                            
                            var tth = ImageToBuffer(image, ImageFormat.Jpeg);
                            var length = getImageName.Split('_').Length;
                            string name = "";
                            for (int i = 1; i < length; i++)
                            {
                                name += getImageName.Split('_')[i];
                            }
                            //name = name.Split('.')[0];
                            var fileContent = new ByteArrayContent(tth);
                            fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = name };
                            formDataContent.Add(fileContent);
                            formDataContent.Add(new StringContent(form["Name"]), "Name");
                            formDataContent.Add(new StringContent(form["Price"]), "Price");
                            formDataContent.Add(new StringContent(form["Quantity"]), "Quantity");
                            formDataContent.Add(new StringContent(GetProductPublishDate(id)), "PublishDate");
                            formDataContent.Add(new StringContent(form["Description"]), "Description");
                            formDataContent.Add(new StringContent(GetProductCategoryId(id)), "CategoryId");
                            formDataContent.Add(new StringContent(form["Status"]), "Status");
                            formDataContent.Add(new StringContent(form["Id"]), "ProductId");
                            var requestUri = "http://10.11.24.95/Intern/Product/Edit/" + Session["id"];
                            var result = client.PostAsync(requestUri, formDataContent).Result;
                            if (result.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                string a = result.Content.ReadAsStringAsync().Result;
                            }
                            else
                            {
                                ViewBag.Msg = "上傳失敗! 請重試或聯絡克服";
                            }
                        }
                    }
                    ViewBag.Msg = "修改成功";
                    return RedirectToAction("ProductManagement", "Member");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    ViewBag.Msg = "伺服器問題，請稍後重試";
                    return RedirectToAction("ProductManagement", "Member");
                }
            }
        }

        public byte[] imgToByteArray(Image image)
        {
            using(MemoryStream memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, image.RawFormat);
                return memoryStream.ToArray();
            }
        }

        public static byte[] ImageToBuffer(Image Image, System.Drawing.Imaging.ImageFormat imageFormat)
        {
            if (Image == null) { return null; }
            byte[] data = null;
            using (MemoryStream oMemoryStream = new MemoryStream())
            {
                //建立副本
                using (Bitmap oBitmap = new Bitmap(Image))
                {
                    //儲存圖片到 MemoryStream 物件，並且指定儲存影像之格式
                    oBitmap.Save(oMemoryStream, imageFormat);
                    //設定資料流位置
                    oMemoryStream.Position = 0;
                    //設定 buffer 長度
                    data = new byte[oMemoryStream.Length];
                    //將資料寫入 buffer
                    oMemoryStream.Read(data, 0, Convert.ToInt32(oMemoryStream.Length));
                    //將所有緩衝區的資料寫入資料流
                    oMemoryStream.Flush();
                }
            }
            return data;
        }

        public Image DownloadImageFromUrl(string imageUrl)
        {
            Image image = null;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                WebResponse webResponse = webRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                image = Image.FromStream(stream);
                webResponse.Close();
            }catch(Exception ex)
            {
                return null;
            }

            return image;
        }
    

        //public bool PostDataWithNoFile(string id, FormCollection collection)
        //{
        //    try
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            using (var formDataContent = new MultipartFormDataContent())
        //            {
        //                byte[] Bytes = new byte[file.InputStream.Length + 1];
        //                file.InputStream.Read(Bytes, 0, Bytes.Length);
        //                var fileContent = new ByteArrayContent(Bytes);
        //                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
        //                formDataContent.Add(fileContent);
        //                formDataContent.Add(new StringContent(collection["Name"]), "Name");
        //                formDataContent.Add(new StringContent(collection["Price"]), "Price");
        //                formDataContent.Add(new StringContent(collection["Quantity"]), "Quantity");
        //                formDataContent.Add(new StringContent(GetProductPublishDate(id)), "PublishDate");
        //                formDataContent.Add(new StringContent(collection["Description"]), "Description");
        //                formDataContent.Add(new StringContent(GetProductCategoryId(id)), "CategoryId");
        //                formDataContent.Add(new StringContent(collection["Status"]), "Status");
        //                formDataContent.Add(new StringContent(Session["id"].ToString()), "Id");
        //                var requestUri = "http://10.11.24.95/Intern/Product/Edit/" + Session["id"];
        //                var result = client.PostAsync(requestUri, formDataContent).Result;
        //                if (result.StatusCode == System.Net.HttpStatusCode.OK)
        //                {
        //                    string a = result.Content.ReadAsStringAsync().Result;
        //                }
        //                else
        //                {
        //                    ViewBag.Msg = "上傳失敗! 請重試或聯絡克服";
        //                }
        //            }
        //        }
        //        return true;
        //    }
        //    catch (Exception e)
        //    {

        //        Console.WriteLine(e.ToString());
        //        ViewBag.Msg = "伺服器問題，請稍後重試";
        //        return false;
        //    }
        //}
    
        public bool PostData(System.Web.Mvc.FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                string id = collection["Id"];
                using (var client = new HttpClient())
                {
                    using (var formDataContent = new MultipartFormDataContent())
                    {
                        byte[] Bytes = new byte[file.InputStream.Length + 1];
                        file.InputStream.Read(Bytes, 0, Bytes.Length);
                        var fileContent = new ByteArrayContent(Bytes);
                        fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                        formDataContent.Add(fileContent);
                        formDataContent.Add(new StringContent(collection["Name"]), "Name");
                        formDataContent.Add(new StringContent(collection["Price"]), "Price");
                        formDataContent.Add(new StringContent(collection["Quantity"]), "Quantity");
                        formDataContent.Add(new StringContent(GetProductPublishDate(id)), "PublishDate");
                        formDataContent.Add(new StringContent(collection["Description"]), "Description");
                        formDataContent.Add(new StringContent(GetProductCategoryId(id)), "CategoryId");
                        formDataContent.Add(new StringContent(collection["Status"]), "Status");
                        formDataContent.Add(new StringContent(collection["Id"]), "ProductId");
                        var requestUri = "http://10.11.24.95/Intern/Product/Edit/" + Session["id"];
                        var result = client.PostAsync(requestUri, formDataContent).Result;
                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            string a = result.Content.ReadAsStringAsync().Result;
                        }
                        else
                        {
                            ViewBag.Msg = "上傳失敗! 請重試或聯絡克服";
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
                ViewBag.Msg = "伺服器問題，請稍後重試";
                return false;
            }
        }

        public string GetProductCategoryId(string id)
        {
            try
            {
                var x = "";
                string JsonString = JsonConvert.SerializeObject(x);
                string url = "http://10.11.24.95/Intern/Product/Select/" + id;
                var response = new getApi().PostApi(JsonString, url);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                    string CategoryId = jObject["Result"]["CategoryId"].ToString();
                    return CategoryId;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return "";
            }
        }

        public string GetProductPublishDate(string id)
        {
            try
            {
                var x = "";
                string JsonString = JsonConvert.SerializeObject(x);
                string url = "http://10.11.24.95/Intern/Product/Select/" + id;
                var response = new getApi().PostApi(JsonString, url);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                    string PublishDate = jObject["Result"]["PublishDate"].ToString();
                    return PublishDate;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return "";
            }
        }

        public string GetCategoryName(string code)
        {
            switch (code)
            {
                case "1":
                    return "電腦相關";
                    
                case "2":
                    return "3C";
                    
                case "3":
                    return "家電";
                   
                case "4":
                    return "日常用品";
                    
                case "5":
                    return "生活";
                case "6":
                    return "美妝";
                case "7":
                    return "食品";
                default:
                    return "無類別名稱";
            }
        }
    }
}