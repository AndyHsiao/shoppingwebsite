﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using ShoppingWebsite.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ShoppingWebsite.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "首頁";
            for(int i = 0; i<5; i++)
            {
                string id = new getApi().SalesRank(i);
                ViewData["BestSellerId" + (i + 1)] = id;
                ViewData["Bestseller" + (i + 1)] = Bestseller(id);
                ViewData["ImgUri" + (i+1)] = GetImage(id);
            }
            GetAd();
            int[] product_list = { 3,4,5,1035,1036,1037,1038,1039,1040,1041,1042,1043,1044,1045,1046,1047,1048,1049,1050,1051};
            ViewBag.product_list = product_list;
            GetProductImg(product_list);
            return View();
        }

        [HttpPost]
        public ActionResult AddToCart(string ProductId)
        {
            if (Session["Id"] != null)
            {
                string url = "http://10.11.24.95/Intern/Cart/Add";
                var response = new getApi().PostApiWithHeader("", url, Session["Id"].ToString(), ProductId);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);
                    System.Windows.Forms.MessageBox.Show("已加入購物車", "成功");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public void GetProductImg(int[] list)
        {
            foreach (int i in list)
            {
                var client = new RestClient("http://10.11.24.95/Intern/Product/Select/" + i);
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(response.Content);
                var imgName = jObject["Result"]["DefaultImageId"];
                if (imgName.ToString() == null)
                {
                    ViewData["ProductImage" + i] = "";
                }
                else
                {
                    ViewData["ProductId" + i] = jObject["Result"]["Id"].ToString();
                    ViewData["ProductName" + i] = jObject["Result"]["Name"].ToString();
                    ViewData["ProductPrice" + i] = jObject["Result"]["Price"].ToString();
                    ViewData["ProductImage" + i] = imgName.ToString();
                }
            }
        }

        public void GetAd()
        {
            for (int i = 2002; i <= 2004; i++)
            {
                var client = new RestClient("http://10.11.24.95/Intern/Ad/Select/"+i);
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(response.Content);
                var AdName = jObject["Result"]["Image"];
                var AdUri = jObject["Result"]["Url"];
                ViewData["Ad" + i] = AdName.ToString();
                ViewData["AdUri" + i] = AdUri.ToString();
            }
        }

        public string Bestseller(string productId)
        {
            {
                var x = "";
                string JsonString = JsonConvert.SerializeObject(x);
                string url = "http://10.11.24.95/Intern/Product/Select/" + productId;
                var response = new getApi().PostApi(JsonString, url);

                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                    return jObject["Result"]["Name"].ToString();
                }
                else
                {
                    Debug.WriteLine(response.StatusCode);
                    return "出現錯誤";
                }
            }
        }

        public string GetImage(string productId)
        {
            var client = new RestClient("http://10.11.24.95/Intern/Product/Select/" + productId);
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(response.Content);
            var imgName = jObject["Result"]["DefaultImageId"];
            if (imgName.ToString() == null)
            {
                return "";
            }
            else
            {
                return imgName.ToString();
            }
        }

        public ActionResult Cart()
        {
            ViewBag.Title = "購物車";
            if (Session["Id"] != null)
            {
                GetCartByUser(Session["Id"].ToString());
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public void GetCartByUser(string id)
        {
            var x = "";
            string JsonString = JsonConvert.SerializeObject(x);
            string url = "http://10.11.24.95/Intern/Cart/Select/";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("UserId", id);
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            requestMessage.Content = new StringContent(x, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
            if (response.StatusCode.ToString() == "OK")
            {
                var r = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                int count = jObject["Result"].Count();
                List<CartProduct> cartProducts = new List<CartProduct>(count);
                for (int i = 0; i < count; i++)
                {
                    CartProduct cartProduct = new CartProduct()
                    {
                        ProductId = jObject["Result"][i]["ProductId"].ToString(),
                        ProductName = jObject["Result"][i]["ProductName"].ToString(),
                        Quantity = jObject["Result"][i]["Quantity"].ToString(),
                        Price = jObject["Result"][i]["Price"].ToString()
                    };
                    cartProducts.Add(cartProduct);
                }
                ViewBag.cartProduct = cartProducts;
                ViewBag.TotalAmount = jObject["Message"].ToString();
                //DataTable dt = (DataTable)JsonConvert.DeserializeObject<List<CartProduct>>(jObject["Result"].ToString());
            }
            else
            {
                Debug.WriteLine(response.StatusCode);
            }
        }

        public ActionResult QA()
        {
            ViewBag.Title = "常見問題";
            return View();
        }
    }
}