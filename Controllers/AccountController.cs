﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ShoppingWebsite.Controllers
{
    
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            ViewBag.Title = "登入";
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection login)
        {
            string account = login["account"];
            string password = login["password"];
            if (CheckUserData(account, password))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Msg = "登入失敗...";
                return View(login);
            }
        }

        public bool CheckUserData(string account, string password)
        {
            try
            {
                var acc = new
                {
                    Email = account,
                    Password = password
                };
                string JsonString = JsonConvert.SerializeObject(acc);
                string url = "http://10.11.24.95/Intern/User/UserLogin";
                getApi api = new getApi();
                var response = api.PostApi(JsonString, url);
                if (response.StatusCode.ToString() == "OK")
                {
                    var r = response.Content.ReadAsStringAsync();
                    JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                    Session["Name"] = jObject["Result"]["Name"].ToString();
                    Session["Id"] = jObject["Result"]["Id"];
                    Session["Level"] = jObject["Result"]["Level"].ToString();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public ActionResult Register()
        {
            ViewBag.Title = "註冊";
            return View();
        }

        [HttpPost]
        public ActionResult Register(FormCollection post)
        {
            string password1 = post["password1"];
            string password2 = post["password2"];

            if (string.IsNullOrWhiteSpace(password1) || password1 != password2)
                {
                    ViewBag.Msg = "密碼不一致";
                    return View(post);
                }
            else
            {
                if (AddUserDataAsync(post))
                {
                    TempData["register"] = "註冊成功，請輸入您的帳號密碼登入";
                    Response.Redirect("Login");
                    return new EmptyResult();
                }
                else
                {
                    ViewBag.Msg = "註冊失敗...請重試";
                    return View(post);
                }
            }
        }

        public bool AddUserDataAsync(FormCollection model)
        {
            try
            {
                var data = new
                {
                    Name = model["name"],
                    Email = model["account"],
                    Password = model["password1"],
                    Phone = model["phone"],
                    Level = "買家",
                    address = ""
                };
                string url = "http://10.11.24.95/Intern/User/Create";

                string JsonString = JsonConvert.SerializeObject(data);

                getApi getApi = new getApi();
                var response = getApi.PostApi(JsonString, url);              

                if (response.StatusCode.ToString() == "OK")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}