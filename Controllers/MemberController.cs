﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ShoppingWebsite.Controllers
{
    public class MemberController : Controller
    {
        public ActionResult Member()
        {
            ViewBag.Title = "會員專區";
            if (Session["Id"] != null)
            {
                GetMemberInfo();
                return View();
            } 
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        private void GetMemberInfo()
        {
            int id = Convert.ToInt32(Session["Id"]);
            getApi getApi = new getApi();
            
            var response = getApi.GetMemberInfo(id);
            if (response.StatusCode.ToString() == "OK")
            {
                var r = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(r.Result);
                ViewBag.name = jObject["Result"]["Name"].ToString();
                ViewBag.email = jObject["Result"]["Email"].ToString();
                ViewBag.password = jObject["Result"]["Password"].ToString();
                ViewBag.phone = jObject["Result"]["Phone"].ToString();
                ViewBag.level = jObject["Result"]["Level"].ToString();
                ViewBag.address = jObject["Result"]["Address"].ToString();
            }
            else
            {
                Debug.WriteLine("Response : " + response.Content.ReadAsStringAsync());
            }
        }

        public ActionResult ProductManagement()
        {
            ViewBag.Title = "商品管理";
            if (Session["Id"] != null)
            {
                GetUserProducts();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public void GetUserProducts()
        {
            var client = new RestClient("http://10.11.24.95/Intern/Product/SelectByUser/" + Session["id"]);// + Session["id"]);
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                JObject jObject = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JObject>(response.Content);
                int count = jObject["Result"].Count();
                ViewBag.Count = count;
                for (int i = 1; i <= count; i++)
                {
                    ViewData["P_Id" + i] = jObject["Result"][i - 1]["Id"].ToString();
                    ViewData["ProductId" + i] = jObject["Result"][i - 1]["Id"].ToString();
                    ViewData["ProductName" + i] = jObject["Result"][i - 1]["Name"];
                    ViewData["ProductPrice" + i] = jObject["Result"][i - 1]["Price"];
                    ViewData["ProductUri" + i] = jObject["Result"][i - 1]["DefaultImageId"];
                }
            }
        }

        public ActionResult DeleteProduct(string ProductId)
        {
            string url = "http://10.11.24.95/Intern/Product/Delete/";
            url += ProductId;
            var response = new getApi().PostApi("", url);
            if (response.StatusCode.ToString() == "Ok")
            {
                var r = response.Content.ReadAsStringAsync();
                JObject jObject = JsonConvert.DeserializeObject<JObject>(r.Result);
                ViewData["DeleteMessage"] = "已刪除一個產品";
                return RedirectToAction("ProductManagement", "Member");
            }
            else
            {
                ViewData["DeleteMessage"] = "刪除失敗，請重新操作";
                return RedirectToAction("ProductManagement", "Member");
            }
        }

        public ActionResult FinancialManagement()
        {
            ViewBag.Title = "進帳管理";
            return View();
        }

        public ActionResult AddProduct()
        {
            ViewBag.Title = "新增商品";
            if (Session["Id"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult StoreManagement()
        {
            ViewBag.Title = "新增商品";
            if (Session["Id"] != null)
            {
                GetMemberInfo();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult ChangeInfo(FormCollection form)
        {
            GetMemberInfo();
            var info = new
            {
                Id = Session["Id"],
                Name = form["name"],
                Password = ViewBag.password,
                Email = ViewBag.email,
                Phone = form["phone"],
                Level = ViewBag.level,
                address = form["address"]
            };
            string JsonString = JsonConvert.SerializeObject(info);
            Session["Name"] = form["name"];
            if (ChangeUserData(JsonString))
            {
                GetMemberInfo();
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return View(form);
            }
        }

        public bool ChangeUserData (string info)
        {
            try
            {
                string url = "http://10.11.24.95/Intern/User/Edit";
                getApi api = new getApi();
                var response = api.PostApi(info, url);
                if (response.StatusCode.ToString() == "OK")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


        [HttpPost]
        public ActionResult AddProduct(FormCollection form, HttpPostedFileBase file)
        {
            if (PostData(form, file))
            {
               
                return View();
            }
            else
            {
                
                return View(form);
            }
        }

        public bool PostData(FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    using (var client = new HttpClient())
                    {
                        using (var formDataContent = new MultipartFormDataContent())
                        {
                            byte[] Bytes = new byte[file.InputStream.Length + 1];
                            file.InputStream.Read(Bytes, 0, Bytes.Length);
                            var fileContent = new ByteArrayContent(Bytes);
                            fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                            formDataContent.Add(fileContent);
                            formDataContent.Add(new StringContent(collection["Name"]), "Name");
                            formDataContent.Add(new StringContent(collection["Price"]), "Price");
                            formDataContent.Add(new StringContent(collection["Quantity"]), "Quantity");
                            formDataContent.Add(new StringContent(DateTime.Now.ToString("MM-dd-yyyy")), "PublishDate");
                            formDataContent.Add(new StringContent(collection["Description"]), "Description");
                            formDataContent.Add(new StringContent(collection["CategoryId"]), "CategoryId");
                            formDataContent.Add(new StringContent(collection["Status"]), "Status");
                            var requestUri = "http://10.11.24.95/Intern/Product/Create/" + Session["id"];
                            var result = client.PostAsync(requestUri, formDataContent).Result;
                            if (result.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                string a = result.Content.ReadAsStringAsync().Result;
                                //List<string> m = result.Content.ReadAsAsync<List<string>>().Result;
                                //ViewBag.Msg = m.FirstOrDefault();
                                ViewBag.Msg = "上傳成功!";
                            }
                            else
                            {
                                ViewBag.Msg = "上傳失敗! 請重試或聯絡克服";
                            }
                        }
                    }
                    return true;
                }
                else
                {
                    ViewBag.Msg = "請上傳圖片";
                    return false;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
                ViewBag.Msg = "伺服器問題，請稍後重試";
                return false;
            }
        }
    }
}